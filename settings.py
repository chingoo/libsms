SMS_TRANSPORTS = {
    'default': {
        'BAKEND': 'libsms.backends.sms.SmsTransport',
        'PARAMS': {
            'login': 'some_login',
            'password': 'some_password',
        }
    },
    'dummy': {
        'BAKEND': 'libsms.backends.dummy.SmsTransport',
    },
    'other': {
        'BAKEND': 'libsms.backends.other.SmsTransport',
        'PARAMS': {
            'login': 'some_login',
            'password': 'some_password',
            'var1': 'var1',
            'var2': 'var2',
        }
    }
}
