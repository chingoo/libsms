from importlib import import_module
import settings

__all__ = ['sms_transport', 'sms_transports']


class BadSMSTConfig(Exception):
    pass


class SMSTransportCore():
    def __init__(self):
        try:
            self.config = settings.SMS_TRANSPORTS
        except AttributeError:
            raise BadSMSTConfig('SMS_TRANSPORTS not found in settings')
        if not self.config.get('default'):
            raise BadSMSTConfig('Default param not found in SMS_TRANSPORTS')

    def __getitem__(self, item):
        br = self._parseconfig(item)
        return getattr(import_module(br['path']), br['name'])(**br['params'])

    def _parseconfig(self, item):
        """Parsing SMS_TRANSPORTS config block"""
        current = self.config.get(item)
        if not current:
            raise BadSMSTConfig('Alias <{}> not found in SMS_TRANSPORTS'.
                                format(item))
        backend = current.get('BAKEND')
        if not backend:
            raise BadSMSTConfig('Backend not found in <{}> block'.format(item))
        p, n = backend.rsplit('.', 1)
        return {'path': p, 'name': n, 'params': current.get('PARAMS', {})}

sms_transports = SMSTransportCore()
sms_transport = sms_transports['default']
