from . import BaseSMSTransport


class SmsTransport(BaseSMSTransport):
    def __init__(self, **kwargs):
        super().__init__()
        self.params = kwargs

    def send(self, phone, msg):
        print('Phone: {}, Message: {}, Params: {}'.format(phone, msg,
                                                          self.params))
