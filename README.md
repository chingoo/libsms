## libsms

**TEST Workout**

### Install

    git clone https://bitbucket.org/chingoo/libsms.git

### Use

    from libsms import sms_transport
    from libsms import sms_transports

    sms_transport.send(phone='123123', msg='qweqwe')
    sms_transports['dummy'].send(phone='123123', msg='qweqwe')
    
    >> Phone: 123123, Message: qweqwe, Params: {'login': 'some_login', 'password': 'some_password'}
    >> Phone: 123123, Message: qweqwe, Params: {}

### Example

    python3 libsms/run.py

### Uninstall

    rm -rf libsms
    
###### PS

Cause of none environment needed as is, no deploy implemented.
